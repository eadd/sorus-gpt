//% color="#0051a4" iconWidth=50 iconHeight=40
namespace SoruxGPT{
    //% block="初始化SoruxGPT" blockType="command"
    export function SoruxGPT(parameter: any, block: any) {
        Generator.addImport(`import requests\nimport json\nfrom PIL import Image`)
        Generator.addCode(`def SoruxGPT_txt():
    try:
    # 发送POST请求
        response = requests.post(url=url,headers=headers, data=json_str)
        json_data = response.json()
        #print(json_data)
        txt=json_data['choices'][0]['message']['content']
        return txt # 打印返回的报文内容   
    except Exception as e:
        return('Error occurred: ', str(e))
def SoruxGPT_wst(filename,new_width,new_height):
    # 发送POST请求
    response = requests.post(url=url2,headers=headers, data=json_str2)
    json_data2 = response.json()      
    image_url=json_data2['data'][0]['url']        
    response = requests.get(image_url)
    with open(filename, 'wb') as file:# 打开本地文件进行写入操作
        file.write(response.content)# 将图片内容写入本地文件
    image = Image.open(filename)    
    resized_image = image.resize((new_width, new_height))
    # 保存修改后的图片
    resized_image.save(filename)    
        `)
    }
    //% block="SoruxGPT—API密钥:[SS]" blockType="command"
    //% SS.shadow="normal" SS.defl="XXXX"
    export function SoruxGPT_model(parameter: any, block: any) {
        let X=parameter.SS.code
        Generator.addCode(`headers = {
            'Content-Type': 'application/json', # 根据需要设置其他header信息
            'Authorization': 'Bearer ${X}' # 在这里设置Authorization头部的值
    }`)

    } 

    //% block="SoruxGPT文生文的引导词:[TT]和输入内容[Content]" blockType="command"
    //% TT.shadow="normal" TT.defl="中文"
    //% Content.shadow="normal" Content.defl="'鸟为什么会飞?'"
    export function SoruxGPT_wsw(parameter: any, block: any) {
        let Y=parameter.TT.code
        let Cont=parameter.Content.code
        Generator.addCode(`data={
            "model": "gpt-3.5-turbo",
            "messages": [
              {
                "role": "system",
                "content":'${Y}'
              },
              {
                "role": "user",
                "content":${Cont}
              }
        ]
    }
json_str = json.dumps(data)
url='https://gpt.soruxgpt.com/api/api/v1/chat/completions'`)
    } 

    //% block="获取SoruxGPT-文生文返回的值" blockType="reporter"
    export function keras_Img(parameter: any, block: any) {
       
        Generator.addCode(`SoruxGPT_txt()`)
    } 


    //% block="SoruxGPT文生图引导词:[TxT]" blockType="command"
    //% TxT.shadow="normal" TxT.defl="'河边，沙滩，水鸟，鱼儿'"
    export function SoruxGPT_wst(parameter: any, block: any) {
        let Y=parameter.TxT.code
        Generator.addCode(`data2={
            "model":"dall-e-3",
            "prompt":${Y},
            "n":1                       
        }
json_str2 = json.dumps(data2)
url2='https://gpt.soruxgpt.com/api/api/v1/images/generations'
`)} 
    //% block="获取SoruxGPT-文生图返回的图片：[filename],宽[W],高[H]" blockType="command"
    //% filename.shadow="normal" filename.defl="local_image.jpg"
    //% W.shadow="normal" W.defl="1024"
    //% H.shadow="normal" H.defl="1024"
    export function SoruxGPT_Img(parameter: any, block: any) {
    let filename=parameter.filename.code 
    let new_width=parameter.W.code 
    let new_height=parameter.H.code 
    Generator.addCode(`SoruxGPT_wst("${filename}",${new_width},${new_height})`)
} 

}
